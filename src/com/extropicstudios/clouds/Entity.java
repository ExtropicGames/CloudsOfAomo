package com.extropicstudios.clouds;

import java.awt.Point;

import net.slashie.libjcsi.CSIColor;

public class Entity {
	public String name;
	public char icon;
	public int x;
	public int y;
	public CSIColor color;
	
	public Entity() {
		name = "Formless figure";
		icon = '%';
		x = y = 0;
		color = CSIColor.BONDI_BLUE;
	}
	
	public Entity(String newName, char newIcon) {
		name = newName;
		icon = newIcon;
	}
	
	public void set(String newName, char newIcon) {
		name = newName;
		icon = newIcon;
	}
	
	public void move(int newX, int newY) {
		x = newX;
		y = newY;
	}
	
	public void move(Point location) {
		x = location.x;
		y = location.y;
	}
}
