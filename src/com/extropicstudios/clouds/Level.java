package com.extropicstudios.clouds;

import java.awt.Point;

import com.extropicstudios.clouds.util.RandomNumberGenerator;

import net.slashie.libjcsi.CSIColor;

public class Level {
	public int width;
	public int height;
	public char[][] tiles;
	public CSIColor[][] colors;
	public CSIColor default_floor_color;
	public CSIColor default_wall_color;
	public CSIColor default_bg_color;
	
	private RandomNumberGenerator rng;
	
	public Level(int width, int height, RandomNumberGenerator rng) {
		this.width = width;
		this.height = height;
		this.rng = rng;
		clear();
	}
	
	public void setTheme(CSIColor floor, CSIColor wall, CSIColor bg) {
		default_floor_color = floor;
		default_wall_color = wall;
		default_bg_color = bg;
	}
	
	public void clear() {
		tiles = new char[width][height];
		colors = new CSIColor[width][height];
	}
	
	public void putFloor(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height)
			return;
		tiles[x][y] = '.';
		colors[x][y] = default_floor_color;
	}
	
	public void putWall(int x, int y) {
		tiles[x][y] = '#';
		colors[x][y] = default_wall_color;
	}
	
	public Point findOpenFloor() {
		int attempts = 0;
		// first try to find one randomly
		while (attempts < 1000) {
			int rx = rng.nextInt(width);
			int ry = rng.nextInt(height);
			if (tiles[rx][ry] == '.') {
				return new Point(rx, ry);
			}
			attempts++;
		}
		
		// if we can't, do it iteratively
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				if (tiles[ix][iy] == '.') {
					return new Point(ix, iy);
				}
			}
		}
		// couldn't find it, no open tiles exist
		return null;
	}
}
