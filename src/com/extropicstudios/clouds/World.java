package com.extropicstudios.clouds;

import java.awt.Point;
import java.util.ArrayList;

import com.extropicstudios.clouds.levgen.LevelGenerator;
import com.extropicstudios.clouds.util.RandomNumberGenerator;

import net.slashie.libjcsi.CSIColor;
import net.slashie.libjcsi.CharKey;
import net.slashie.libjcsi.wswing.WSwingConsoleInterface;

// Tile types:
// '.' = floor
// '#' = wall
// ' ' = sky

public class World {
	
	// data
	private static Level lev;
	private static ArrayList<Entity> creatures;
	private static Player pc = new Player();
	private static CSIColor bgColor;
	private static final RandomNumberGenerator rng = new RandomNumberGenerator();
	
	// interfaces
	static WSwingConsoleInterface console = new WSwingConsoleInterface("Skies of Aomo", false);
	private static final LevelGenerator levgen = new LevelGenerator(rng);
	
	/** Level related functions **/
	
	private static void Refresh() {
		// wipe the map and all creatures
		// except the player
		lev.clear();
		creatures = new ArrayList<Entity>();
	}

	public static void GenerateLevel() {
		// wipe the old level
		Refresh();
		
		bgColor = levgen.generateLevel(lev);
		
		// generate new levels until we get one that works
		boolean done = false;
		while (!done) {
			Point openSpace = lev.findOpenFloor();
			if (openSpace != null) {
				pc.move(openSpace);
				done = true;
			}
			else
				bgColor = levgen.generateLevel(lev);
		}
		
		// TODO: generate some random entities and put them
		// in here.

	}
	
	/* Entity related functions */

	public static boolean movePlayer(char direction) {
		int newX = pc.x;
		int newY = pc.y;
		if (direction == 'a')
			newX--;
		else if (direction == 'd')
			newX++;
		else if (direction == 'w')
			newY--;
		else if (direction == 's')
			newY++;
		else
			return false;
		if (!isBlocked(newX, newY)) {
			pc.move(newX, newY);
			return true;
		}
		return false;
	}
	
	public static boolean isBlocked(int x, int y) {
		if (x < 0 || y < 0 || x >= WSwingConsoleInterface.xdim || y >= WSwingConsoleInterface.ydim)
			return true;
		if (lev.tiles[x][y] != '#') {
			if (pc.x == x && pc.y == y)
				return true;
            for (Entity creature : creatures) {
                if (creature.x == x && creature.y == y) {
                    return true;
                }
            }
			return false;
		}
		return true;
	}
	
	/** I/O related functions **/
	
	public static void renderScreen() {
		// draw the map
		for (int x = 0; x < lev.width; x++) {
			for (int y = 0; y < lev.height; y++) {
				console.print(x,y,lev.tiles[x][y], lev.colors[x][y], bgColor);
			}
		}
		
		// draw the creatures
        for (Entity c : creatures) {
            console.print(c.x, c.y, c.icon, c.color, bgColor);
        }
		
		// draw the PC
		console.print(pc.x, pc.y, pc.icon, pc.color, bgColor);
		
		console.refresh();
	}
	
	public static boolean checkInput() {
		int key = console.inkey().code;
		if (key == CharKey.q || key == CharKey.Q || key == CharKey.ESC)
			return true;
		else if (key == CharKey.DARROW || key == CharKey.s || key == CharKey.S)
			movePlayer('s');
		else if (key == CharKey.UARROW)
			movePlayer('w');
		else if (key == CharKey.LARROW) 
			movePlayer('a');
		else if (key == CharKey.RARROW)
			movePlayer('d');
		return false;
	}
	
	static void sleep(int milliseconds) {
		try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {}
	}
	
	public static void main(String[] args) {
		lev = new Level(WSwingConsoleInterface.xdim, WSwingConsoleInterface.ydim, rng);
		GenerateLevel();
		// pretty simple main game loop...
		// 1. check for player input and act on it
		// 2. if the screen changes, update it
		// 3. frame delay
		boolean quit = false;
		while (!quit) {
			renderScreen();
			quit = checkInput();
			//sleep(75);
		}
		System.exit(0);
	}
}
