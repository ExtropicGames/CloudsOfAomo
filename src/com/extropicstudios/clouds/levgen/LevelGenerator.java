package com.extropicstudios.clouds.levgen;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.extropicstudios.clouds.Level;
import com.extropicstudios.clouds.util.RandomNumberGenerator;
import com.extropicstudios.clouds.util.Util;
import com.extropicstudios.enums.Direction;

import static com.extropicstudios.enums.Direction.*;

import net.slashie.libjcsi.CSIColor;

public class LevelGenerator {
	// random number generation constants
	private static final int NUM_THEMES = 5;
	private static final int MIN_NUM_ROOMS = 5; // will keep adding more rooms until
												// at least this many exist
	private static final int ROOM_CHANCE = 20; // the lower this is, the more rooms
											   // will be generated on average
	private static final int CORRIDOR_CHANCE = 2; // the lower this is, the more likely it is
												  // that a room will have a corridor
	private static final int CORRIDOR_TWISTINESS = 10; // the higher this is, the more the corridors
													   // will twist and turn
	
	// interfaces
	private RandomNumberGenerator rng;
	
	public LevelGenerator(RandomNumberGenerator rng) {
		this.rng = rng;
	}
	
	public CSIColor generateLevel(Level level) {
		int col = rng.nextInt(NUM_THEMES);
		
		// pick a theme
		if (col == 0)
			level.setTheme(CSIColor.PINK, CSIColor.RED, CSIColor.BLUE);
		else if (col == 1)
			level.setTheme(CSIColor.BLUE, CSIColor.YELLOW, CSIColor.GRAY);
		else
			level.setTheme(CSIColor.BLACK, CSIColor.BLACK, CSIColor.WHITE);
		
		// set the map to all walls
		for (int x = 0; x < level.width; x++) {
			for (int y = 0; y < level.height; y++) {
				level.tiles[x][y] = '#';
				level.colors[x][y] = level.default_wall_color;
			}
		}
		
		// TODO: Room generation needs some serious cleanup.
		
		// generate rooms
		List<Room> roomList = new ArrayList<Room>();
		while (rng.nextInt(ROOM_CHANCE) != 0 && roomList.size() < MIN_NUM_ROOMS) {
			Room room = makeRoom(level);
			if (room != null)
				roomList.add(room);
		}
		
		for (int i = 0; i < roomList.size(); i++) {
			// each room has a 50% chance to have a corridor sticking out
			// of it. this doesn't count incoming corridors from other rooms.
			if (rng.nextInt(CORRIDOR_CHANCE) == 0) {
				Room target = roomList.get(rng.nextInt(roomList.size()));
				roomList.get(i).addLink(target);
				target.addLink(roomList.get(i));
				makeCorridor(level, roomList.get(i), target);
			}
		}
		
		// every room must have at least one incoming or outgoing corridor
		for (Room room : roomList) {
			if (room.getLinks().isEmpty()) {
				Room target = roomList.get(rng.nextInt(roomList.size()));
				room.addLink(target);
				target.addLink(room);
				makeCorridor(level, room, target);
			}
		}
		
		// then we want to cull wall tiles so that there is empty air outside the rooms.
		// we are in the sky, after all
		
		for (int x = 0; x < level.width; x++) {
			for (int y = 0; y < level.height; y++) {
				boolean allWalls = true;
				for (int i = (x-1); i <= (x+1); i++) {
					for (int j = (y-1); j <= (y+1); j++) {
						if (!(i < 0 || j < 0 || i >= level.width || j >= level.height)) {
							if (level.tiles[i][j] == '.')
								allWalls = false;
						}
					}
				}
				if (allWalls) {
					level.tiles[x][y] = ' ';
				}
			}
		}
		
		return level.default_bg_color;
	}
	
	private void makeCorridor(Level level, Room startRoom, Room endRoom) {
		// corridor generation needs even more cleanup
		
		// pick random points in each room to be the start and end points
		// of the corridor
		Point start = rng.getRandomPointInRectangle(startRoom.getArea());
		Point end = rng.getRandomPointInRectangle(endRoom.getArea());
		
		int cursorX = start.x;
		int cursorY = start.y;
		cursorX = Util.setInRange(1, cursorX, (level.width - 2));
		cursorY = Util.setInRange(1, cursorY, (level.height - 2));
		Direction direction;
		int corridorLength = 0;
		int numTurns = rng.nextInt(CORRIDOR_TWISTINESS);
		while (numTurns > 0) {
			direction = rng.getRandomDirection();
			if (direction == NORTH && cursorY > 1) {
				corridorLength = rng.nextInt(cursorY);
				for (int i = 0; i < corridorLength; i++)
					level.putFloor(cursorX, cursorY - i);
				cursorY =- corridorLength;
			}
			if (direction == SOUTH && cursorY < (level.height - 1)) {
				corridorLength = rng.nextInt(level.height - cursorY);
				for (int i = 0; i < corridorLength; i++)
					level.putFloor(cursorX, cursorY + i);
				cursorY =+ corridorLength;
			}
			if (direction == EAST && cursorX < (level.width - 1)) {
				corridorLength = rng.nextInt(level.width - cursorX);
				for (int i = 0; i < corridorLength; i++)
					level.putFloor(cursorX + i, cursorY);
				cursorX =+ corridorLength;
			}
			if (direction == WEST && cursorX > 1) {
				corridorLength = rng.nextInt(cursorX);
				for (int i = 0; i < corridorLength; i++)
					level.putFloor(cursorX - i, cursorY);
				cursorX =- corridorLength;
			}
			numTurns--;
		}
	}
	
	private Room makeRoom(Level level) {
		int attempts = 0;
		while (attempts < 1000) {
			// pick a random point on the map
			int x = rng.nextInt(level.width - 1) + 1;
			int y = rng.nextInt(level.height - 1) + 1;
			int w = rng.nextInt(6) + 2;
			int h = rng.nextInt(6) + 2;
			// make sure the room doesn't extend off the map
			if ((x + w) < (level.width - 1) && (y+h) < (level.height - 1)) {
				// plop down a room
				for (int i = x; i < (x + w); i++) {
					for (int j = y; j < (y + h); j++) {
						level.tiles[i][j] = '.';
						level.colors[i][j] = level.default_floor_color;
					}
				}
				return new Room(x,y,w,h);
			}
			attempts++;
		}
		return null;
	}
}