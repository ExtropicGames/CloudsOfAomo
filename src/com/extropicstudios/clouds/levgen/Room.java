package com.extropicstudios.clouds.levgen;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Room {

	// change this to be a sort of 1-bit bitmap cutout type thing
	private Rectangle area;
	
	private String particle;
	private String adjective;
	private String description;

	private final List<Room> links = new ArrayList<Room>();

	public Room(int x, int y, int w, int h) {
		area = new Rectangle(x,y,w,h);
	}
	
	public Rectangle getArea() {
		return area;
	}
	
	public void addLink(final Room target) {
		links.add(target);
	}
	
	public List<Room> getLinks() {
		return Collections.unmodifiableList(links);
	}
	
	public String toString() {
		return particle + " " + adjective + " " + description;
	}
}
