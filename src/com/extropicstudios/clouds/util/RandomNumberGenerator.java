package com.extropicstudios.clouds.util;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Random;

import com.extropicstudios.enums.Direction;

public class RandomNumberGenerator {

	private Random rand = new Random();
	
	public Point getRandomPointInRectangle(Rectangle rect) {
		int x = rect.x + rand.nextInt(rect.width + 1);
		int y = rect.y + rand.nextInt(rect.height + 1);
		return new Point(x,y);
	}
	
	public int nextInt(int n) {
		return rand.nextInt(n);
	}
	
	public Direction getRandomDirection() {
		int n = rand.nextInt(4);
		if (n == 0)
			return Direction.NORTH;
		else if (n == 1)
			return Direction.SOUTH;
		else if (n == 2)
			return Direction.EAST;
		else
			return Direction.WEST;
	}
}
