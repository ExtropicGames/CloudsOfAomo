package com.extropicstudios.clouds.util;

public class Util {

	public static int setInRange(int min, int value, int max) {
		if (value <= min)
			return min;
		if (value >= max)
			return max;
		return value;
	}
}
