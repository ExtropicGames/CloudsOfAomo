package com.extropicstudios.enums;

public enum Direction {
	NORTH,
	SOUTH,
	EAST,
	WEST,
}
